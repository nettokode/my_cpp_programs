#include <iostream>
#include <cmath>

using namespace std;

double calc_det(double a, double b, double c) {
    double det = 0.0;
    det = (b * b) - (4 * a * c);
    if(det <= 0) {
        return 0.0;
    } else {
        return sqrt(det);
    }
}

double calc_eq_1(double det, double a, double b) {
    double x_1 = 0.0;
    b *= -1;
    x_1 = (b + det)/(2*a);
    return x_1;
}


double calc_eq_2(double det, double a, double b) {
    double x_2 = 0.0;
    b *= -1;
    x_2 = (b - det)/(2*a);
    return x_2;
}

int main() {
    double a = 0.0;
    double b = 0.0;
    double c = 0.0;
    double det_1 = 0.0;
    double r1 = 0.0;
    double r2 = 0.0;

    cin >> a;
    cin >> b;
    cin >> c;

    cout << fixed;
    cout.precision(5);

    det_1 = calc_det(a, b, c);

    if(det_1 == 0.0 || a == 0.0) {
        cout << "Impossivel calcular" << '\n';
    } else {
        r1 = calc_eq_1(det_1, a, b);
        r2 = calc_eq_2(det_1, a, b);
        cout << "R1 = " << r1 << '\n';
        cout << "R2 = " << r2 << '\n';
    }

    return 0;
}
