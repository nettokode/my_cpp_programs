#include <iostream>
#include <cmath>

using namespace std;

//const double pi 3.14159
#define PI 3.14159;

int main() {
    double raio = 0.0;
    double area = 0.0;

    cout << fixed;
    cout.precision(4);
    cin >> raio;
    area = pow(raio, 2)*PI;
    cout << "A=" << area << '\n';

    return 0;
}
