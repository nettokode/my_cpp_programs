#include <iostream>

using namespace std;

int main() {
    float c_quente = 4.00;
    float x_salada = 4.50;
    float x_bacon = 5.0;
    float torrada = 2.0;
    float refrigerante = 1.50;
    float resultado = 0.0;

    int produto = 0;
    float quantidade = 0;

    cout << fixed;
    cout.precision(2);

    cin >>  produto;
    cin >> quantidade;
    switch (produto) {
        case 1:
            resultado = c_quente * quantidade;
            break;
        case 2:
            resultado = x_salada * quantidade;
            break;
        case 3:
            resultado = quantidade * x_bacon;
            break;
        case 4:
            resultado = torrada * quantidade;
            break;
        case 5:
            resultado = refrigerante * quantidade;
            break;
    }
    cout << "Total: R$ " << resultado << '\n';
    return 0;
}
