#include <iostream>
#include "carro.hpp"

using namespace std;

int main(){
    Carro *tornado = new Carro(250, "vermelho", "tornado", "esportivo");
    Carro *luxo = new Carro(200, "preto", "luxo", "carro de luxo");

    tornado->setVelMax(300);
    cout << tornado->getVelMax() << "\n";

    return 0;
}