#include <iostream>

using namespace std;

class Espada{
    public:
        float larg;
        float comp;
        string nome;
        void init(float e_larg, float e_comp, string nome);
        void show();

    private:

};
void Espada::init(float e_larg, float e_comp, string e_nome){
    this->larg = e_larg;
    this->comp = e_comp;
    this->nome = e_nome;
}
void Espada::show(){
    cout << this->larg << "\n";
    cout << this->comp << "\n";
    cout << this->nome << "\n";
}

int main(){
    Espada *escalibur = new Espada();
    
    escalibur->init(0.5, 1.2, "esclibur");
    escalibur->show();

    return 0;
}
