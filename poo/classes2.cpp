#include <iostream>

using namespace std;

class Gato{
    public:
        string corDoPelo;
        string corDosOlhos;
        string nome;
        string temperamento;

        void init(string corDoPelo, string corDosOlhos, string nome, string temperamento);
        void correr();
        void miar();
        void ronronar();

    private:
};

void Gato::init(string e_corDoPelo, string e_corDosOlhos, string e_nome, string e_temperamento){
    this->corDoPelo = e_corDoPelo;
    this->corDosOlhos = e_corDosOlhos;
    this->nome = e_nome;
    this->temperamento = e_temperamento;
}
void Gato::correr(){
    cout << this->nome << " esta correndo!" << "\n";
}
void Gato::miar(){
    cout << this->nome << " esta miando!" << "\n";
}
void Gato::ronronar(){
    cout << this->nome << " esta ronronando!" << "\n";
}

int main(){
    Gato *bichano = new Gato();
    bichano->init("branco", "verde", "bichano", "calmo");
    bichano->correr();
    bichano->miar();
    bichano->ronronar();

    return 0;
}