#include <iostream>

using namespace std;

struct espada{
    float larg;
    float comp;
    string name;

    void init(float e_larg, float e_comp, string e_name){
        larg = e_larg;
        comp = e_comp;
        name = e_name;
    }

    void show(){
        cout << larg << "\n";
        cout << comp <<"\n";
        cout << name << "\n";
    }
};

int main(){
    espada escalibur;
    escalibur.init(0.5, 1.2, "escalibur");
    escalibur.show();

    return 0;
}