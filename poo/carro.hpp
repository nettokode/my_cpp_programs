#define CARRO_H
#include <iostream>

using namespace std;

class Carro{
    public:
        int getVelMax();
        void setVelMax(int n);

        string getCor();
        void setCor(string cr);

        string getNome();
        void setNome(string nm);

        string getTipo();
        void setTipo(string tp);

        Carro(int velm, string cr, string nm, string tp);
        void show();

    private:
        int velMax;
        string cor;
        string nome;
        string tipo;
};

Carro::Carro(int velm, string cr, string nm, string tp){
    velMax = velm;
    cor = cr;
    nome = nm;
    tipo = tp;
}

void Carro::show(){
    cout << "Velocidade maxima: " << this->velMax << "\n";
    cout << "Cor..............: " << this->cor << "\n";
    cout << "Nome.............: " << this->nome << "\n";
    cout << "Tipo.............: " << this->tipo << "\n";
}

int Carro::getVelMax(){
    return this->velMax;
}

void Carro::setVelMax(int n){
    this->velMax = n;
}

string Carro::getCor(){
    return this->cor;
}

void Carro::setCor(string cr){
    this->cor = cr;
}

string Carro::getNome(){
    return this->nome;
}

void Carro::setNome(string nm){
    this->nome = nm;
}

string Carro::getTipo(){
    return this->tipo;
}

void Carro::setTipo(string tp){
    this->tipo = tp;
}
