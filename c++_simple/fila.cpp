#include <iostream>
#include <queue>

using namespace std;

int main(){
    queue <string> armas;

    //Adiciona elementos na fila
    armas.push("espada");
    armas.push("faca");
    armas.push("lança");

    //Remove e retorna o elemento da frente da fila(queue)
    armas.pop();

    //Retorna o tamanho da fila(queue)
    cout << armas.size() << "\n";

    //Retorna verdadeiro se a fila(queue) estiver vazia
    cout << armas.empty() << "\n";

    //Retorna o ultimo elemento da fila(queue)
    cout << armas.back() << "\n";

    //Retorna o primeiro elemento da fila(queue)
    cout << armas.front() << "\n";

    return 0;
}