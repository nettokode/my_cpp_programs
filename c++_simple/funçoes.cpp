#include <iostream>

using namespace std;

// void fib(int n);
void print_name(string name);
void print_name(string name, string s_name);
void print_name(string name, string s_name, string ps_name);

int main(){

    print_name("neto");
    print_name("neto", "diniz");
    print_name("neto", "gomes", "diniz");

    return 0;
}


/*
void fib(int n){
    int a = 0;
    int b = 1;
    int c = 0;
    cout << a << '\n';

    for(int i=0; i<n; i++){
        cout << b << '\n';
        c = a;
        a = b;
        b = a + c;
    }
}
*/

void print_name(string name){
    cout << name << '\n';
}

void print_name(string name, string s_name){
    cout << name << ' ' << s_name << '\n';
}

void print_name(string name, string s_name, string ps_name){
    cout << name << ' ' << s_name << ' ' << ps_name << '\n';
}