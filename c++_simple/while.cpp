#include <iostream>

using namespace std;

int main() {
/*
//LAÇO WHILE

    int n = 0;
    while (n < 11) {
        cout << n << '\n';
        n++;
*/

/*
//LAÇO DO WHILE
    int n = 0;
    do {
        n++;
        cout << n << '\n';
    } while(n < 10);
*/

    for (size_t i = 10; i > 0 ; i--) {
        cout << i <<'\n';
    }
}
