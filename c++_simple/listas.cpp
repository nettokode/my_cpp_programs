#include <iostream>
#include <list>

using namespace std;

int main(){
    //Instacia uma nova lista
    list <string> armas;
    //Instacia um novo iterator
    list <string>::iterator it;

    //Adiciona um elemento no inicio da lista
    armas.push_front("lança");
    armas.push_front("faca");
    armas.push_front("spada");
    armas.push_front("arco");

    // lança[3], faca[2], espada[1], arco[0]
    // lança[4], faca[3], espada[2], arco[1], flecha[0]

    //Adiciona um elemento no final da lista
    // armas.push_back("espada");
    // armas.push_back("arco");

    it = armas.begin();
    advance(it, 1);
    armas.insert(it, "flecha");
    
    it = armas.begin();
    advance(it, 1);
    
    //Apaga a posiçao definida no iterator
    armas.erase(it);
    int tam = armas.size();

    //Retorna o primeiro elemento da lista
    for(int i=0; i<tam; i++){
        cout << armas.front() << "\n";
        armas.pop_front();
    }

/*
    //Retorna o ultimo elemento da lista
    //cout << armas.back() << "\n";

    //Remove o primeiro elemento da lista
    armas.pop_front();

    //Remove o ultimo elemento da lista
    armas.pop_back();

    //Retorna o numero de elementos da lista
    cout << armas.size() << "\n";
    
    //Remove um elemento indicado da lista
    armas.remove("espada");

    //Ordena os elementos da lista
    armas.sort();
    
    //Reverte a ordem dos elementos da lista
    armas.reverse();

    //Remove todos os elementos da lista
    armas.clear();
*/
    return 0;
}