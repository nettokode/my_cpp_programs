#include <iostream>

using namespace std;

struct carro{
    string nome;
    string cor;
    int velmax;
    int pot;

    void init(string st_nome, string st_cor, int st_velmax, int st_pot){
        nome = st_nome;
        cor = st_cor;
        velmax = st_velmax;
        pot = st_pot;
    }
};

int main(){
    carro carro1;
    
    carro1.init("ferrari", "vermelho", 300, 500);
    cout << carro1.nome << "\n";
    cout << carro1.cor << "\n";
    cout << carro1.velmax << "\n";
    cout << carro1.pot << "\n";
        
    return 0;
}