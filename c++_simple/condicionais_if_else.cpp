#include <iostream>

using namespace std;

int main() {
/*
    int n1 = 0;
    int n2 = 0;

    cout << "Digite um numero: ";
    cin >> n1;

    cout << "Digite outro numero: ";
    cin >> n2;

    if (n1%2 == 0) {
        cout << "O primeiro numero e par" << '\n';
    } else {
        cout << "O primeiro numero e impar" << '\n';
    }

    if (n2%2 == 0) {
        cout << "O segundo numero e par" << '\n';
    } else {
        cout << "O segundo numero é impar" << '\n';
    }
*/
    int n1 = 0;
    int n2 = 0;

    cout << "Digite um numero: " << '\n';
    cin >> n1;

    cout << "Digite um outro numero" << '\n';
    cin >> n2;

    if (n1 > n2) {
        cout << "O primeiro valor é maior" << '\n';
    } else if (n2 > n1){
        cout << "O segundo valor é maior" << '\n';
    } else {
        cout << "Os valores sao iguais" << '\n';
    }

    return 0;
}
