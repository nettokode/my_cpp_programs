#include <iostream>
#include <vector>

using namespace std;

int main(){
    vector <int> nums1, nums2;

    nums1.push_back(0);
    nums1.push_back(1);
    nums1.push_back(2);
    nums1.push_back(3);


    nums2.push_back(4);
    nums2.push_back(5);
    nums2.push_back(6);
    nums2.push_back(7);

    nums1.swap(nums2);

    int tam = nums1.size();
    for(int i=0; i<tam; i++){
        cout << nums1[i] << "\n";
    }
}