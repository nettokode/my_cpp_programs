#include <iostream>

using namespace std;

int main(){
    //Cria um novo vetor de <Char> types: <string> <int> <double>;
    char vector[4];

    vector[0] = 'n';
    vector[1] = 'e';
    vector[2] = 't';
    vector[3] = 'o';
    
    cout << vector[0];
    cout << vector[1];
    cout << vector[2];
    cout << vector[3];

    return 0;
}