#include <iostream>
#include <stack>

using namespace std;

int main(){
    //Cria uma nova pilha(stack)
    stack <string> baralho;
    
    //Adiciona itens na pilha(stack)
    baralho.push("Q");
    baralho.push("K");
    baralho.push("J");

    //Retorna o tamanho da pilha(stack)
    cout << baralho.size();

    //Remova elementos da pilha(stack)
    baralho.pop();

    //Verifica se a pilha(stack) esta vazia
    baralho.empty();

    return 0;
}